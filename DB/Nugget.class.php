<?php
    require_once('MarioDal.class.php');
    class Nugget extends MarioDal {

        public function __construct() {
            parent::__construct();

            // Define table properties
            $this->_columnValues = array('NuggetId' => 0, 'Code' => '', 'OwnerUserId' => 0, 'Content' => '', 'DateCreated' => '', 'DateUpdated' => '');
            $this->_tableName = 'Nuggets';
            $this->_primaryKey = 'NuggetId';
        }

        public function loadNugget($code) {
            $rows = $this->loadByColumn('Code', $code);
            $this->_columnValues = $rows[0];
            return true;
        }

        public function generateRandomCode($len=8) {
            $rc = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", $len)), 0, $len);
            return $rc;
        }

    }
