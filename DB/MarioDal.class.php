<?php
/***********************************************************/
/* MarioDal is a simple object based DAL for mysql and PHP */
/***********************************************************/
    abstract class MarioDal {
        protected $_databaseHost;
        protected $_databasePort;
        protected $_databaseName;
        protected $_databaseUser;
        protected $_databasePassword;

        protected $_columnValues;
        protected $_tableName;
        protected $_primaryKey;

        protected $_saveMode; // on save, determines if is update or insert "insert" or "update"

        protected $_debug = true;

        public function set($column, $val) {
            $this->_columnValues[$column] = $val;
        }

        public function get($column) {
            return $this->_columnValues[$column];
        }

        protected function load($val) {
            return $this->loadByColumn($this->_primaryKey, $val, $type);
        }

        protected function loadByColumn($column, $val) {
            $sql = "select * from $this->_tableName where $column = :$column";
            $params[":$column"] = $val;
            $this->_saveMode = "update";
            return $this->pdoMarioReadQuery($sql, $params);
        }

        protected function delete() {
            // refactor this when you need it!!!
            $sql = "delete from " . $this->getTableName() . " where " . $this->_primaryKey . " = '" . $this->_columnValues[$this->_primaryKey] . "'";
            $this->marioQuery($sql);
        }

        protected function __construct() {
            include('db.php');
            $this->_databaseHost     = $dbcredentials['host'];
            $this->_databaseUser     = $dbcredentials['user'];
            $this->_databasePassword = $dbcredentials['pass'];
            $this->_databaseName     = $dbcredentials['name'];
            $this->_databasePort     = $dbcredentials['port'];

            $this->_saveMode = "insert";
        }

        public function save() {
            $funcCall = $this->_saveMode;
            $this->$funcCall();
            $this->_saveMode = "update";
        }

        private function insert() {
            $sql = "";
            $currentColumns = $this->_columnValues;
            unset($currentColumns[$this->_primaryKey]);

            $params = array();
            $columns = "";
            $values = "";
            $first = true;

            foreach($currentColumns as $key=>$value) {
                if($first) {
                    $columns .= "$key";
                    $values .= ":$key";
                    $first = false;
                } else {
                    $columns .= ",$key";
                    $values .= ",:$key";
                }
                $params[":$key"] = $value;
            }
            $sql = "insert into $this->_tableName($columns) values($values)";
            $this->pdoMarioQuery($sql, $params);
        }

        private function update() {
            $sql = "";
            $columnUpdates = "";
            $currentColumns = $this->_columnValues;
            unset($currentColumns[$this->_primaryKey]);

            $params = array();
            $columns = "";
            $values = "";
            $first = true;

            foreach($currentColumns as $key=>$value) {
                if($first) {
                    $columnUpdates .= "$key = :$key";
                    $first = false;
                } else {
                    $columnUpdates .= ", $key = :$key";
                }
                $params[":$key"] = $value;
            }
            $params[":NuggetId"] = $this->_columnValues[$this->_primaryKey];
            $sql = "update $this->_tableName set $columnUpdates where $this->_primaryKey = :$this->_primaryKey";
            $this->pdoMarioQuery($sql, $params);
        }

        private function pdoMarioQuery($query, $params) {
            if(!is_array($params)) {
                throw new Exception("pdoMarioQuery: params must be array");
            }
            $conn = new PDO("mysql:host=" . $this->_databaseHost . ";dbname=" . $this->_databaseName . ";port=" . $this->_databasePort ,$this->_databaseUser,$this->_databasePassword);
            $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $statement = $conn->prepare($query);
            $res = $statement->execute($params);
        }

        private function pdoMarioReadQuery($query, $params) {
            if(!is_array($params)) {
                throw new Exception("pdoMarioReadQuery: params must be array");
            }
            $conn = new PDO("mysql:host=" . $this->_databaseHost . ";dbname=" . $this->_databaseName . ";port=" . $this->_databasePort ,$this->_databaseUser,$this->_databasePassword);
            $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $statement = $conn->prepare($query);
            $rc = array();
            if($statement->execute($params)) {
                while($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $rc[] = $row;
                }
            }
            return $rc;
        }

        private function log($message) {
            if(!$this->_debug) {
                exit;
            }
            $date = date('m/d/Y h:i:s a', time());
            $message = $date . ' - ' . $message . PHP_EOL;
            file_put_contents('./log/system.log', $message, FILE_APPEND);
        }

        private function marioQuery($query) {
            echo "Depricated function call. Refactor code now!";
            print_r(debug_backtrace());
            die();
        }

        private function marioReadQuery($query) {
            echo "Depricated function call. Refactor code now!";
            print_r(debug_backtrace());
            die();
            
            $this->log("Processing query (readquery) = " . $query);
            try {
                $mysqli = new mysqli($this->_databaseHost, $this->_databaseUser, $this->_databasePassword, $this->_databaseName, $this->_databasePort);
            } catch (Exception $e) {
                $this->log("Exception on connection (readquery) - " . $e->message);
            }
            if(mysqli_connect_errno()) {
                $this->log("mysqli error (readquery) - " . mysqli_connect_error());
                exit;
            }
            $result = $mysqli->query($query);
            if(!$result) {
                $this->log("No results returned");
                return null;
            }
            $this->log("Query result (readquery): " . print_r($result, true));
            $rc = array();
            try {
                while($row = $result->fetch_assoc()) {
                    $rc[] = $row;
                }
            } catch (Exception $e) {
                $this->log("Exception on processing rows (readquery) - " . $e->message);
            }
            return $rc;
        }
    }
