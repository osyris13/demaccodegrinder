<?php
    require_once('MarioDal.class.php');
    class User extends MarioDal {
        protected function __construct() {
            parent::__construct();

            // Define table properties
            $this->_columnValues = array('UserId' => 0, 'Email' => '', 'FirstName' => 0, 'LastName' => '', 'Password' => '', 'LastLoginDate' => '');
            $this->_tableName = 'Users';
            $this->_primaryKey = 'UserId';
        }
    }

