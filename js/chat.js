var socket = io.connect('http://localhost:3000');
$('#chat__form').submit(function(){
    socket.emit('chat message', $('#chat__input').val());
    $('#chat__input').val('');
    return false;
});
socket.on('chat message', function(msg){
    var chatHistory = document.getElementByClassName('chat-history');
    var chatAtBottom = true;

    if (chatHistory.scrollHeight - chatHistory.scrollTop() == chatHistory.outerHeight()) {
        chatAtBottom = true;
    } else {
        chatAtBottom = false;
    }
    alert('test');
    console.log(chatAtBottom);
    $('.chat-history').append($('<p>').text(msg));

    if (chatAtBottom){
        chatHistory.scrollTop() = chatHistory.scrollHeight();
    }
});
