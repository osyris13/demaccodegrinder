// Global variables
var theme = '';

// app
;(function($, undefined) {
    'use strict';
    // global app declaration
    window.grind = {};
    /**
    * app "global" vars
    */
    grind.hasTouch = 'ontouchstart' in document;
    grind.w = document.documentElement.clientWidth;
    grind.isMobile = grind.w < 768;
    grind.isTablet = grind.w < 1024 && grind.w >= 768;
})(jQuery);


// initThemeSelector
;(function($, undefined) {
    'use strict';

    grind.initThemeSelector = function() {

        var input = $('#select-theme');
        function selectTheme() {
            theme = input.val();
            editor.setOption("theme", theme);
            $('head').append('<link rel="stylesheet" href="theme/' + theme + '.css">')
        }
        var choice = (location.hash && location.hash.slice(1)) || (document.location.search && decodeURIComponent(document.location.search.slice(1)));
        if (choice) {
            input.value = choice;
            editor.setOption("theme", choice);
        }

        $('#select-theme').on('change', function(){
            selectTheme();
        })

        selectTheme();
    };
})(jQuery);

// initModeSelect
;(function($, undefined){
    'use strict';

    grind.initModeSelect = function() {
        var mode = $('#select-mode').val();

        function change() {
            if (mode) {
                editor.setOption("mode", mode);
                CodeMirror.autoLoadMode(editor, mode);
            } else {
                alert("Could not find a mode corresponding to " + mode);
            }
        }

        $('#select-mode').on('change', function() {
            CodeMirror.modeURL = "../mode/%N/%N.js";
            mode = $(this).val();
            $('#hdnLanguage').val(mode);
            change();
        })
    }
})(jQuery);

// initPrefsSelect
;(function($, undefined) {
    'use strict';

    grind.initPrefsSelect = function() {
        $('.settings-tab').on('click', function(){
            $(this).next().toggleClass('show');
            $(this).parents('#options-sidebar').toggleClass('open');
            $(this).parent().toggleClass('active');
        })
    };
})(jQuery);

// initTagsInput
;(function($, undefined) {
    'use strict';

    grind.initTagsInput = function() {
        $('#tags-btn').on('click', function(){

            if ($(this).parents('#footer-toolbar').hasClass('open-for-tags')){
                $(this).parents('#footer-toolbar').removeClass('open-for-tags');
                $(this).parents('#footer-toolbar').find('#tags-input__wrapper').removeClass('show').addClass('hide');
                $(this).text('Tags +');
            } else {
                $(this).parents('#footer-toolbar').find('#tags-input__wrapper').removeClass('hide').addClass('show');
                $(this).parents('#footer-toolbar').addClass('open-for-tags');
                $(this).text('Tags -');
            }

        })
    };
})(jQuery);

// initTagsInput
;(function($, undefined) {
    'use strict';

    grind.initChatInput = function() {
        $('#chat-btn').on('click', function(){

            if ($(this).parents('#footer-toolbar').hasClass('open-for-chat')){
                $(this).parents('#footer-toolbar').find('#chat-input__wrapper').removeClass('show').addClass('hide');
                $(this).parents('#footer-toolbar').removeClass('open-for-chat');
                $(this).text('Chat +');
            } else {
                $(this).parents('#footer-toolbar').find('#chat-input__wrapper').removeClass('hide').addClass('show');
                $(this).parents('#footer-toolbar').addClass('open-for-chat');
                $(this).text('Chat -');
            }

        })
    };
})(jQuery);

// User markup and addition
;(function($, undefined) {
    'use strict';

    grind.initUserSignin = function() {
        
    };
})(jQuery);

// init
;(function($, undefined) {
    'use strict';

    grind.init = function() {
        // Initialize events
        grind.initThemeSelector();
        grind.initPrefsSelect();
        grind.initModeSelect();
        grind.initTagsInput();
        grind.initChatInput();
        grind.initUserSignin();
    };

    $(grind.init);
})(jQuery);
