<?php
logerror("save.php - in save.php");
require_once('DB/Nugget.class.php');
logerror("save.php - in save.php, after require");
if(isset($_POST['hdnForm'])) {
	logerror("save.php - inside if");
	$pageCode = $_POST['hdnCode'];
	$pageContent = $_POST['code'];
	$currentTime = time();
	$currentNugget = new Nugget();
	logerror("save.php - setting values");
	if($pageCode == '0') {
		$pageCode = $currentNugget->generateRandomCode();
		$currentNugget->set('OwnerUserId', '0');
		$currentNugget->set('DateCreated', $currentTime);
		$currentNugget->set('Code', $pageCode); 
	} else {
		$currentNugget->loadNugget($pageCode);
	}
	$currentNugget->set('Content', $_POST['code']);
	$currentNugget->set('LanguageMode', $_POST['hdnLanguage']);
	logerror("code to save:@@@" . $_POST['code']);
	$currentNugget->set('DateUpdated', $currentTime);
	logerror("save.php - values set. About to save");
	$currentNugget->save();
	logerror("save.php - nugget saved");

	header('Location: '. 'index.php?id=' . $pageCode);
} else {
	logerror("save.php - skipped if");
}

function logerror($message) {
	$date = date('m/d/Y h:i:s a', time());
	$message = $date . ' - ' . $message . PHP_EOL;
	file_put_contents('log/system.log', $message, FILE_APPEND);
}